import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import{ConnectionService} from '../../services/connection.service';
import {AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollection,AngularFirestoreDocument, AngularFirestoreModule } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { viewClassName } from '@angular/compiler';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})


export class CreateComponent implements OnInit {
  itm:any={name:'', descripcion:'', image:''}
  uploadPercent: Observable<number>;
  urlImage:Observable<string>;
  @ViewChild('imageProduct') inputImageProduct:ElementRef;

constructor(private con:ConnectionService, private storage: AngularFireStorage) { }


  ngOnInit(): void {
  }

  create(){       
    this.itm.image= this.inputImageProduct.nativeElement.value;  
    this.con.addItem(this.itm);
       
  }

  handleImage(event) {

    const id = Math.random().toString(36).substring(2);
    const file = event.target.files[0];
    const filePath = `upload/${id}`;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);
    task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();

  }

    

}
