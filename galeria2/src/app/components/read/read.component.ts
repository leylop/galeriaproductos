import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import{ConnectionService} from '../../services/connection.service';
import { AngularFirestore, AngularFirestoreCollection,AngularFirestoreDocument, AngularFirestoreModule } from '@angular/fire/firestore';
import {AngularFireStorage } from '@angular/fire/storage';
import { map, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { viewClassName } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {

  itm:any;
  itemEdit:any={name:'', descripcion:'',image:''}
  urlImage:Observable<string>;
  @ViewChild('imageProducte') inputImageProduct:ElementRef;

  constructor(private con:ConnectionService, private storage: AngularFireStorage ) { 
   
    
    this.con.returnItems().subscribe(items=>{
      this.itm=items;
      console.log(this.itm);

    })
  }

  ngOnInit(): void {
  }

  delete(id) {
    this.con.deleteItem(id);
  }
 edit(item){     
   this.itemEdit=item;
  
 }

 editForm(){
this.itemEdit.image= this.inputImageProduct.nativeElement.value;
this.con.editItem(this.itemEdit)
 }

 updateImage(event) {

  const id = Math.random().toString(36).substring(2);
  const file = event.target.files[0];
  const filePath = `upload/${id}`;
  const ref = this.storage.ref(filePath);
  const task = ref.put(file);
  task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();

}

// showCreate(){
//   alert()
// }

}
